package moronya.todo.web.rest;


import moronya.todo.domain.User;
import moronya.todo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/api")
public class UserResource {
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(UserResource.class);


    public UserResource(UserService userService) {
        this.userService = userService;
    }
    @PostMapping("/user")
    public ResponseEntity<String> save(@RequestBody User user) throws MessagingException {
        logger.debug("Request to save user");

        userService.saveUser(user);

        // we return a ResponseEntity to avoid transporting the password in the dto...
        return new ResponseEntity<>("User was created", HttpStatus.OK);

    }

}
