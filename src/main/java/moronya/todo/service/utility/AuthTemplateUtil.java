package moronya.todo.service.utility;

public class AuthTemplateUtil {

    public static String getAccountCreationEmail(String name, String companyName, String phoneNumber){
        return "<!doctype html>" +
                "<html>" +
                "<head> <meta name=\"viewport\" content=\"width=device-width\"> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> <title>Activation Code</title> " +
                "<style>#outer-table{background-color: #f1f3f4; font-family: CenturyGothicStd, \"Century Gothic\", CenturyGothic, AppleGothic, sans-serif !important; line-height: 20px; color: #202124; width: 100%;}#content-data{width: 500px; margin: auto; background-color: white; padding: 20px; border-radius: 8px;}#logo-table{width: 100%;}.ext-link{color: white !important; background-color: #18224B; padding: 10px 20px; text-decoration: none; border-radius: 50px; text-transform: uppercase; font-size: small; font-weight: 600; letter-spacing: 1px;}#footer{color: #80868B; font-size: small;}</style>" +
                "</head>" +
                "<body> <table id=\"outer-table\"> <tbody> <th> <td> <p>&nbsp;</p></td></th> <tr> <td colspan=\"1\"></td><td colspan=\"1\" id=\"content-data\"> <table id=\"logo-table\"> <tbody> <tr> <td colspan=\"1\">" + companyName+  "</td><td align=\"right\" colspan=\"1\"> </td></tr></tbody> </table> <hr/> " +
                "<h3> Hello " + name + ", welcome to " + companyName+  ". </h3> " +
                "<p>We are excited you joined us. We help you schedule your tasks and plan your day to increase productivity.</p><br/> " +
                "<p> <i><strong>NOTE:</strong> This is a welcome email. </i> </p><p> <strong>Don't know why you're getting this email? </strong> <br/> Someone has registered an account in " + companyName +  " with this email.<br/> If you do not wish to maintain your account with us, ignore this email and the account will be deleted in 24 hours. </p><hr/> <p id=\"footer\" align=center> ColonyKe, "  +phoneNumber + "</p></td><td colspan=\"1\"></td></tr><th> <td> <p>&nbsp;</p></td></th> </tbody> </table></body></html>";
    }
}
